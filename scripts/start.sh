#!/bin/bash

python3 manage.py migrate

python3 manage.py collectstatic --no-input

gunicorn app_core.wsgi --bind 0.0.0.0:8000
